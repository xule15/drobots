#!/usr/bin/python -u
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('-I. --all Services.ice')  
import drobots 
import services



class PlayerIntf(drobots.Player):
	
	def __init__(self,adapter):
		self.adapter = adapter
		self.contador=1

	def makeController(self,bot,current=None):

		print("Numero de robot ",str(self.contador))
		
		proxyContainerDrobot = miDrobot.communicator().propertyToProxy("ContainerDrobot") 
		
		objetoContainer = services.ContainerPrx.checkedCast(proxyContainerDrobot)
		
		diccProxyFact=objetoContainer.list()

		if(self.contador<4):

			proxyFactoria=diccProxyFact["Factoria"+str(self.contador)]
			
		else:
		
			proxyFactoria=diccProxyFact["Factoria2"]
			
		objetoFactoria = services.FactoryPrx.checkedCast(proxyFactoria) 

		robot=objetoFactoria.make(bot,self.contador)		

		self.contador=self.contador+1

		return robot

	def makeDetectorController(self,current=None):
		
		proxyContainerDetector = miDrobot.communicator().propertyToProxy("ContainerDetector") 
		
		objetoContainerDetector = services.ContainerPrx.checkedCast(proxyContainerDetector)
		
		diccProxyDetector=objetoContainerDetector.list()

		proxyDetector=diccProxyDetector["Factoria3"]

		print("Proxy del detector ",str(diccProxyDetector["Factoria3"]))

		objetoDetector = services.FactoryPrx.checkedCast(proxyDetector) 
		
		detector=objetoDetector.makeDetector()
		
		self.contador=self.contador+1
		
		return detector
			
	def win(self,current=None):

		print ("WIN")
		current.adapter.getCommunicator().shutdown()
		

	def lose(self,current=None):

		print ("LOSE")
		current.adapter.getCommunicator().shutdown()

	
	def gameAbort(self,current=None):

		print("El servidor ha abortado el juego")
		current.adapter.getCommunicator().shutdown()

class miDrobot(Ice.Application):
	def run(self, argv):

		nick=str(argv[1])

		proxy = self.communicator().propertyToProxy("ServidorGame")  

		objetoGame = drobots.GamePrx.checkedCast(proxy)  
		
		if not objetoGame:
			raise RuntimeError('Invalid proxy')

		broker = self.communicator()

		adapter = broker.createObjectAdapter("AdaptadorGame")

		adapter.activate()
       
		sirvientePlayer = PlayerIntf(adapter)   

		proxyPlayer = adapter.addWithUUID(sirvientePlayer)
		
		sys.stdout.flush()

		player= drobots.PlayerPrx.uncheckedCast(proxyPlayer)
		
		sys.stdout.flush()

		objetoGame.login(player, nick)
		
		self.shutdownOnInterrupt()

		broker.waitForShutdown()

		return 0

sys.exit(miDrobot().main(sys.argv))
