#!/usr/bin/python -u
# -*- coding: utf-8 -*-

import sys
import Ice
Ice.loadSlice('-I. --all Services.ice')  
import drobots
import services
import random

class  attackerControllerIntF(drobots.RobotController):

	def __init__(self, bot):
		self.bot=bot

	def turn(self,current=None):
		
		pos = self.bot.location()
		energia= self.bot.energy()
		azar=random.randint(1, 10)

		if 10<=pos.x<=199 and 10<=pos.y<=199:
			azarDisparo=random.randint(270,369)
			if azar % 2 == 0:
				self.bot.drive(270,80)
			else:
				self.bot.cannon(azarDisparo,150)
			

		if 200<=pos.x<=389 and 10<=pos.y<=199:
			azarDisparo=random.randint(0,89)
			if azar % 2 == 0:
				self.bot.drive(0,80)
			else:
				self.bot.cannon(azarDisparo,150)
			

		if 10<=pos.x<=199 and 200<=pos.y<=389:
			azarDisparo=random.randint(90,179)
			if azar % 2 == 0:
				self.bot.drive(90,80)
			else:
				self.bot.cannon(azarDisparo,150)
			

		if 200<=pos.x<=389 and 200<=pos.y<=389:
			azarDisparo=random.randint(180,269)
			if azar % 2 == 0:
				self.bot.drive(180,80)
			else:
				self.bot.cannon(azarDisparo,150)
			

		if 0<=pos.x<10:
			azarDisparo=random.randint(270,369)
			if azar % 2 == 0:
				self.bot.drive(270,80)
			else:
				self.bot.cannon(azarDisparo,150)

		if 0<=pos.y<10:
			azarDisparo=random.randint(0,89)
			if azar % 2 == 0:
				self.bot.drive(0,80)
			else:
				self.bot.cannon(azarDisparo,150)

		if 389<=pos.x<399:
			azarDisparo=random.randint(90,179)
			if azar % 2 == 0:
				self.bot.drive(90,80)
			else:
				self.bot.cannon(azarDisparo,150)

		if 389<=pos.y<399:
			azarDisparo=random.randint(180,269)
			if azar % 2 == 0:
				self.bot.drive(180,80)
			else:
				self.bot.cannon(azarDisparo,150)

		
	def robotDestroyed(self,current=None):
		print ("He sido destruido")


class defenderControllerIntF(drobots.RobotController):
	
	def __init__(self, bot):
		self.bot=bot

	def turn(self,current=None):

		pos = self.bot.location()
		energia= self.bot.energy()
		azar=random.randint(1, 10)

		if 10<=pos.x<=199 and 10<=pos.y<=199:
			self.bot.drive(270,90)
			

		if 200<=pos.x<=389 and 10<=pos.y<=199:
			self.bot.drive(0,90)
			

		if 10<=pos.x<=199 and 200<=pos.y<=389:
			self.bot.drive(180,90)
			

		if 200<=pos.x<=389 and 200<=pos.y<=389:
			self.bot.drive(90,90)
			

		if energia>=72:

			if 0<=pos.x<10:
				azarScan=random.randint(270,369)
				if azar % 2 == 0:
					self.bot.drive(270,80)
				else:	
					print("Enemigos Localizados ",self.bot.scan(azarScan,15))

			if 0<=pos.y<10:
				azarScan=random.randint(0,89)
				if azar % 2 == 0:
					self.bot.drive(0,80)
				else:
					print("Enemigos Localizados ",self.bot.scan(azarScan,15))

			if 389<=pos.x<399:
				azarScan=random.randint(90,179)
				if azar % 2 == 0:
					self.bot.drive(90,80)
				else:
					print("Enemigos Localizados ",self.bot.scan(azarScan,15))

			if 389<=pos.y<399:
				azarScan=random.randint(180,269)
				if azar % 2 == 0:
					self.bot.drive(180,80)
				else:
					print("Enemigos Localizados ",self.bot.scan(azarScan,15))

	def robotDestroyed(self,current=None):
		print ("He sido destruido")


class detectorControllerIntF(drobots.DetectorController):

	def alert(self,pos,enemies,current=None):
		print("Detector de posicion:"+ " " +str(pos) + " "+"y he detectado"+" "+str(enemies)+" "+"enemigos")


class factoriaIntf(services.Factory):

	def __init__(self,adapter):
		self.adapter = adapter

	def make(self, bot,nb,current=None):

		if bot.ice_isA("::drobots::Attacker"):			
			
			sirvienteRobotController= attackerControllerIntF(bot)			
			
		else:
			
			sirvienteRobotController= defenderControllerIntF(bot)

		proxymakeController = current.adapter.addWithUUID(sirvienteRobotController)
			
		proxyContainerRobotController = factoria.communicator().propertyToProxy("ContainerRobotController")

		objetoContainerRobotController = services.ContainerPrx.checkedCast(proxyContainerRobotController) 

		objetoContainerRobotController.link(str(nb), proxymakeController) #añadimos con la clave nb.

		makeController = drobots.RobotControllerPrx.uncheckedCast(proxymakeController)

		return makeController

	def makeDetector(self,current=None):

		sirvienteDetectorController= detectorControllerIntF()

		proxymakeDetector = current.adapter.addWithUUID(sirvienteDetectorController)
			
		proxyContainerDetectorController = factoria.communicator().propertyToProxy("ContainerDetectorController")

		contadorDetector=0

		contadorDetector=contadorDetector+1

		objetoContainerDetectorController = services.ContainerPrx.checkedCast(proxyContainerDetectorController) 

		objetoContainerDetectorController.link(str(contadorDetector), proxymakeDetector)
		
		detectorController = drobots.DetectorControllerPrx.uncheckedCast(proxymakeDetector)

		return detectorController

class factoria(Ice.Application):

	def run(self, argv):
		
		broker = self.communicator()

		adapter = broker.createObjectAdapter("AdaptadorFactoria")

		adapter.activate()
       
		sirvienteFactoria = factoriaIntf(adapter)

		proxyFactoria = adapter.addWithUUID(sirvienteFactoria)
		
		factoryKey="Factoria"+argv[1]

		print("FactoryKey: \t",factoryKey)

		sys.stdout.flush()

		proxyContainerFactoria = self.communicator().propertyToProxy("ContainerFactoria")

		objetoContainerfactoria = services.ContainerPrx.checkedCast(proxyContainerFactoria) 

		objetoContainerfactoria.link(factoryKey, proxyFactoria) 
		
		self.shutdownOnInterrupt()

		broker.waitForShutdown()

		return 0


sys.exit(factoria().main(sys.argv))
	
