#!/usr/bin/python -u


import sys
import Ice
Ice.loadSlice('Services.ice --all -I .')
import drobots
import services


class ContainerIntf(services.Container):
    def __init__(self):
        self.proxies = dict()

    def link(self, key, proxy,current=None):
        if key in dict():
            raise drobots.AlreadyExists(key)

        self.proxies[key] = proxy

    def unlink(self, key, current=None):
        if not key in self.proxies:
            raise drobots.NoSuchKey(key)

        del self.proxies[key]

    def list(self, current=None):
        return self.proxies


class container(Ice.Application):

    def run(self, argv):

        broker = self.communicator()

        servant = ContainerIntf()

        adapterRobotController = broker.createObjectAdapter("ContainerRobotController")

        proxyRobotController = adapterRobotController.add(servant, broker.stringToIdentity("container"))
      
        proxyContainerFactoria = self.communicator().propertyToProxy("ContainerFactoria")

        proxyContainerDetector = self.communicator().propertyToProxy("ContainerDetectorController")


        print("Proxy ", proxyContainerFactoria)
        

        adapterRobotController.activate()
       
        self.shutdownOnInterrupt()

        broker.waitForShutdown()

        return 0

sys.exit(container().main(sys.argv))
