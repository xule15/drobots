# Drobots #

Este proyecto es un juego  realizado en la asignatura Sistemas Distruibuidos.El juego está compuesto
por un servidor que crea partidas a la que se conectan los jugadores. Los jugadores aportan controladores
de robots. Cuando la partida dispone del número de jugadores adecuado crea robots para cada jugador y
les solicita controladores para los mismos. Todos ellos: servidor, jugador, robot y controlador son objetos
distribuidos.
Después, el juego va indicando a cada controlador un turno en el que puede interaccionar con el robot
asociado.Cuando un equipo se queda sin robots ha perdido.